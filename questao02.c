/*
  @author: Nabson Paiva
  @schoolSubject: AEDI - Ciência da Computação
  @question: Implemente funções para realizar as operações de criar,
    inserir e remover elementos em uma fila implementada com lista
    encadeada dinâmica. Considere que os dados armazenados na fila
    são do tipoDados.
*/

typedef struct {
  char nome[20];
  char endereco[40];
  int id;
} tipoDados;

typedef struct tipoNo{
  tipoDados d;
  struct tipoNo *prox;
} tipoNo;

typedef struct {
  tipoNo *prim;
  tipoNo *ultimo;
} tipoLista;

void criar(tipoLista *l){
  l->prim = NULL;
  l->ultimo = NULL;
}

int inserir(tipoLista *l, tipoDados d) {
  tipoNo *aux = (tipoNo *) malloc(sizeof(tipoNo)*8);
  if (aux) {
    aux->d = d;
    aux->prox = NULL;
    if (!l->prim){
      l->prim = aux;
    } else {
      l->ultimo->prox = aux;
    }
    l->ultimo = aux;
    return 1;
  } else return 0;
}

tipoDados remover(tipoLista *l) {
  tipoNo *aux = l->prim;
  tipoDados d;
  if (l->prim) {
    l->prim = l->prim->prox;
    d = aux->d;
    free(aux);
    if (!l->prim)
      l->ultimo = NULL;
    return d;
  }
  return d;
}
