/*
  @author: Nabson Paiva
  @schoolSubject: AEDI - Ciência da Computação
  @question: Corrija a função de inserção em uma lista encadeada descrita no
    código abaixo. A função deve inserir um elemento passado como parâmetro em
    uma lista encadeada também passada como parâmetro. Apresente o código
    corrigido e explique cada uma das alterações realizadas de maneira a ajudar
    o programador que cometeu os erros. Você deve realizar o mínimo possível
    de alterações e apenasas mudanças com devidas explicações serão aceitas.
    A resposta será considerada completamente incorreta se a função corrigida
    apresentar qualquer erro.
*/

void inserir(tipoLista *L, int *d) {
  tipoNo *aux = (tipoNo *) malloc(sizeof(tipoNo)*8);
  if (aux) {
    aux->dado = d;
    aux->prox = L->prim;
    L->prim = aux;
  }
}

/*
  Linha 14: Deve ser passado um ponteiro para a lista encadeada,
    tendo em vista que ela será alterada (campo prim).

  Linha 15: É necessário alocar um espasso de memória para poder
    criar um novo elemento, caso contrário ele seria perdido ao
    fim da função.

  Linha 16: Antes de usar o espaço alocado para o novo nó, precisa-se
    verificar se ele realmente foi alocado e não está apontando para 
    NULL (0).
*/
