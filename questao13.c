/*
  @author: Nabson Paiva
  @schoolSubject: AEDI - Ciência da Computação
  @question: Implemente uma versão modificada do quicksort da questão 10
    para ordenação parcial, onde deseja-se obter os K menores elementos
    de um vetor com N elementos. Considere que será fornecido sempre com
    valor menor que K < N.
*/

int particionar(tipoDados vet[], int ini, int fim){
    tipoDados pivot = vet[fim];
		tipoDados tmp;
    int i = (ini - 1);
    for (int j = ini; j <= fim- 1; j++){
        if (vet[j].id <= pivot.id){
            i++;
						tmp = vet[i];
						vet[i]=vet[j];
						vet[j]=tmp;
        }
    }
		tmp = vet[i+1];
		vet[i+1]=vet[fim];
		vet[fim]=tmp;
    return (i + 1);
}

void quickSort(tipoDados vet[], int ini, int fim,int k) {
    if (ini < fim) {
      int tamP = particionar(vet, ini, fim);
      quickSort(vet,ini,tamP-1,k);
      if(tamP < k-1)
        quickSort(vet,tamP+1,fim,k);
    }
}

void quickSortParcial(tipoDados vet[], int tam, int k) {
	quickSort(vet,0,tam-1,k);
}
