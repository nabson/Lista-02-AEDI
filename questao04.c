/*
  @author: Nabson Paiva
  @schoolSubject: AEDI - Ciência da Computação
  @question: Faça uma função que receba como parâmetro um número K,
  uma lista encadeada circular onde cada nó contenha dados do tipoDados
  e um ponteiro para o próximo elemento, denominado prox. A função deve
  retornar o número de elementos da lista com valor de id igual a K. (P 2006)
*/

int contaID(tipoLista l, int k) {
  int i = 0;
  if (l.atual) {
    tipoNo *aux = l.atual;
    do {
      if (l.atual->d.id == k)
        i++;
      l.atual = l.atual->prox;
    } while(l.atual != aux);
  }
  return i;
}
