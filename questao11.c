/*
  @author: Nabson Paiva
  @schoolSubject: AEDI - Ciência da Computação
  @question: Implemente o algoritmo de ordenação mergesort para
    ordenar um vetor de elementos do tipoDados. A ordenação deve
    ser feita pelo nome.
*/

#include <string.h>

void mergeInt(tipoDados v[], tipoDados r[], int in, int f) {
  int m = (in+f)/2, i, j, k;
  if (in < f) {
    mergeInt(v,r,in,m);
    mergeInt(v,r,m+1,f);
    k = in;
    i = in;
    j = m + 1;
    while (i <= m && j <= f) {
      if (strcmp(v[i].nome,v[j].nome) < 0) {
        r[k] = v[i];
        i++;
      } else {
        r[k] = v[j];
        j++;
      }
      k++;
    }
    while (i <= m) {
      r[k] = v[i];
      i++;
      k++;
    }
    while (j <= f) {
      r[k] = v[j];
      j++;
      k++;
    }
    for (int k=in; k <= f; k++) {
      v[k] = r[k];
    }
  }
}

void mergeSort(tipoDados v[], int t) {
  tipoDados *r = (tipoDados *) malloc(sizeof(tipoDados)*t);
  mergeInt(v,r,0,t-1);
}
