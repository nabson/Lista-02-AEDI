/*
  @author: Nabson Paiva
  @schoolSubject: AEDI - Ciência da Computação
  @question: Implemente funções para realizar as operações de criar,
    inserir e remover elementos em uma pilha implementada com listas
    encadeadas dinâmicas. Considere que os dados armazenados na pilha
    são do tipoDados.
*/

typedef struct {
  char nome[20];
  char endereco[40];
  int id;
} tipoDados;

typedef struct tipoNo{
  tipoDados d;
  struct tipoNo *prox;
} tipoNo;

typedef struct {
  tipoNo *prim;
} tipoLista;

void criar(tipoLista *l){
  l->prim = NULL;
}

int inserir(tipoLista *l, tipoDados d) {
  tipoNo *aux = (tipoNo *) malloc(sizeof(tipoNo)*8);
  if (aux) {
    aux->d = d;
    aux->prox = l->prim;
    l->prim = aux;
    return 1;
  } else return 0;
}

int remover(tipoLista *l, int id) {
  tipoNo *aux, *aux1;
  if (l->prim) {
    aux = l->prim;
    if (aux->d.id == id){
      l->prim = l->prim->prox;
      free(aux);
      return 1;
    }
    while(aux->prox){
      if (aux->prox->d.id == id) {
        aux1 = aux->prox;
        aux->prox = aux->prox->prox;
        free(aux1);
        return 1;
      }
    }
  }
  return 0;
}
