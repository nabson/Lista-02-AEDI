/*
  @author: Nabson Paiva
  @schoolSubject: AEDI - Ciência da Computação
  @question: Faça uma função que receba como parâmetros uma lista encadeada
    com dados do tipoDados e um valor k. A função deve remover o elemento da
    k-ésima posição da lista. Por exemplo, se k= 3, então a função deve remover
    o terceiro elemento da lista. Caso não haja k elementos na lista, a função
    não executa a tarefa.(P 2008)
*/

void removeKesimo(tipoLista *l, int k) {
  if (l->prim) {
    tipoNo *aux = l->prim, *passo;
    if (k == 1) {
      l->prim = l->prim->prox;
      free(aux);
      return;
    }
    k-=2;
    while(k && aux->prox){
      aux = aux->prox;
      k--;
    }
    if (aux->prox) {
      passo = aux->prox;
      aux->prox = aux->prox->prox;
      free(passo);
    }
  }
}
