/*
  @author: Nabson Paiva
  @schoolSubject: AEDI - Ciência da Computação
  @question: Faça uma função que receba como parâmetro uma pilha vazia
    e um vetor de inteiros contendo K posições. A função deve inserir
    na pilha cada valor encontrado no vetor. A ordem de inserção deve
    ser do primeiro para o último elemento do vetor. Os dados da
    pilha neste caso são do tipo int. (P 2006)
*/

typedef struct tipoNo{
  int d;
  struct tipoNo *prox;
} tipoNo;

void vetorPilha(tipoLista *l, int k, int v[k]) {
  tipoNo *aux = (tipoNo *) malloc(sizeof(tipoNo)*8);
  if (aux) {
    aux->d = v[0];
    aux->prox = NULL;
    l->prim = aux;
    for (int i=1; i<k; i++) {
      aux = (tipoNo *) malloc(sizeof(tipoNo)*8);
      if (aux) {
        aux->d = v[i];
        aux->prox = l->prim;
        l->prim = aux;
      } else {
        break;
      }
    }
  }
}
