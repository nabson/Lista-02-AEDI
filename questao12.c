/*
  @author: Nabson Paiva
  @schoolSubject: AEDI - Ciência da Computação
  @question: Implemente o algoritmo de ordenação quicksort para ordenar
    um vetor de elementos do tipoDados. A ordenação deve ser feita pelo id.
*/

void quickInt(tipoDados v[], int in, int f) {
  tipoDados pivo, aux;
  int i, j;
  if (in < f) {
    pivo = v[(in+f)/2];
    i = in;
    j = f;
    do {
      for(;v[i].id < pivo.id;i++);
      for(;v[j].id > pivo.id;j--);
      if (i<=j) {
        aux = v[i];
        v[i] = v[j];
        v[j] = aux;
        i++;
        j--;
      }
    } while (i <= j);
    quickInt(v,in,j);
    quickInt(v,i,f);
  }
}

void quickSort(tipoDados v[], int t) {
  quickInt(v,0,t-1);
}
