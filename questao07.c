/*
  @author: Nabson Paiva
  @schoolSubject: AEDI - Ciência da Computação
  @question: Considere uma lista encadeada dinâmica com dados do tipo
    inteiro (int). Diz-se que uma lista encadeada é simétrica (conceito
    inventado para a questão) se cada elemento que ocorre antes da posição
    do meio (na primeira metade, desconsiderando o elemento do centro se
    houver número ímpar de elementos), tem um correspondente de valor igual
    na segunda metade da lista (há um outro com o mesmo valor na segunda
    metade da lista). Considere que nenhum valor ocorre mais de duas vezes
    na lista. Como exemplo, as listas <3,6,9, 3,9,6> e <6, 3,9,3,9,6> são
    simétricas. Já as listas <3,6,9,3,6,5> e <3,6,5,5,2,3> não são simétricas.
    (P 2008)
*/

typedef struct tipoNo{
  int dados;
  struct tipoNo *prox;
} tipoNo;

int isSimetrica(tipoLista l) {
  tipoNo *meio = l.prim, *in = l.prim;
  int i=0, s1=0,s2=0,m1=1,m2=1;
  for(;l.prim->prox;i++){
    l.prim = l.prim->prox;
  }
  i /= 2;
  for (int v=0; v<=i;v++){
    meio = meio->prox;
  }
  l.prim = in;
  while(meio) {
    s1 += meio->dado;
    m1 *= meio->dado;
    s2 += l.prim->dado;
    m2 *= l.prim->dado;
    meio = meio->prox;
    l.prim = l.prim->prox;
  }
  if (s1 == s2 && m1 == m2)
    return 1;
  else
    return 0;
}
