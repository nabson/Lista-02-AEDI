/*
  @author: Nabson Paiva
  @schoolSubject: AEDI - Ciência da Computação
  @question: Faça uma função para transferir todos os elementos de uma
   lista encadeada dinâmica (com elementos do tipoDados) para um vetor.
   Considere que o vetor comporta todos os elementos da lista. É importante
   que o espaço de memória usado na lista dinâmica seja liberado ao final
   da função. (P 2008)
*/

void transfere(tipoLista *l, int t, tipoDados v[t]) {
  int i = 0;
  tipoNo *aux;
  while(l->prim && i < t) {
    v[i] = l->prim->d;
    i++;
    aux = l->prim;
    l->prim = l->prim->prox;
    free(aux);
  }
}
