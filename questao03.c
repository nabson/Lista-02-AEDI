/*
  @author: Nabson Paiva
  @schoolSubject: AEDI - Ciência da Computação
  @question: Implemente funções para realizar as operações de criar,
   inserir e remover e atualizar ponteiro para atual em uma lista
   circular implementada com lista encadeada dinâmica. Considere que
   os dados armazenados na lista circular são do tipoDados. Considere
   que elementos novos são inseridos depois do atual.
*/

typedef struct {
  char nome[20];
  char endereco[40];
  int id;
} tipoDados;

typedef struct tipoNo{
  tipoDados d;
  struct tipoNo *prox;
} tipoNo;

typedef struct {
  tipoNo *atual;
} tipoLista;

void criar(tipoLista *l){
  l->atual = NULL;
}

int inserir(tipoLista *l, tipoDados d) {
  tipoNo *aux = (tipoNo *) malloc(sizeof(tipoNo)*8);
  if (aux) {
    aux->d = d;
    if (!l->atual){
      l->atual = aux;
      aux->prox = aux;
    } else {
      aux->prox = l->atual->prox;
      l->atual->prox = aux;
      l->atual = aux;
    }
    return 1;
  } else return 0;
}

tipoDados * remover(tipoLista *l, int id) {
  tipoNo *aux, *comeco;
  tipoDados *d = (tipoDados *) malloc(sizeof(tipoDados)*8);
  if (l->atual) {
    comeco = l->atual;
    while (l->atual->prox != comeco) {
      if (l->atual->prox->d.id == id) {
        aux = l->atual->prox;
        l->atual->prox = aux->prox;
        *d = aux->d;
        free(aux);
        l->atual = comeco;
        return d;
      }
      l->atual = l->atual->prox;
    }
    if (comeco->d.id == id) {
      l->atual->prox = comeco->prox;
      l->atual = comeco->prox;
      *d = comeco->d;
      free(comeco);
      return d;
    }
  }
  return NULL;
}

void atualiza(tipoLista *l) {
  l->atual = l->atual->prox;
}
