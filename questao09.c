/*
  @author: Nabson Paiva
  @schoolSubject: AEDI - Ciência da Computação
  @question: Faça uma função para mostrar os elementos de uma fila
    implementada como lista encadeada. Considere que cada nó da fila
    é composto de um campo nome, declarado como um vetor de 50 caracteres.
    A função deve mostrar os elementos na ordem inversa de ocorrência na
    fila e a fila deve ser implementada utilizando lista encadeada simples,
    com ponteiros ligando cada nó ao próximo elemento da fila. A fila deve
    ser percorridauma única vez na função (dica: recursividade). (P 2006)
*/

void imprimeFila(tipoLista l) {
  if (!l.prim->prox){
    printf("%s\n", l.prim->nome);
  } else {
    tipoNo *aux = l.prim;
    l.prim = l.prim->prox;
    imprimeFila(l);
    printf("%s\n", aux->nome);
  }
}
