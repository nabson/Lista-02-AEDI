/*
  @author: Nabson Paiva
  @schoolSubject: AEDI - Ciência da Computação
  @question: Implemente o algoritmo de ordenação QUICKSORT, com chamadas
    para porções menores que 10 elementos executando o algoritmo de
    ordenação por inserção. Considere que o vetor a ser ordenado é de
    elementos do tipoDados, com chave de ordenação sendo o campo nome.
    Considere ainda a possibilidade de haver elementos repetidos no vetor
    a ser ordenado.
*/

#include <string.h>

void quickInt(tipoDados v[], int in, int f) {
  tipoDados pivo, aux;
  int i, j;
  if (f - in > 10) {
    pivo = v[(in+f)/2];
    i = in;
    j = f;
    do {
      for(;strcmp(v[i].nome,pivo.nome)<0;i++);
      for(;strcmp(v[j].nome,pivo.nome)>0;j--);
      if (i<=j) {
        aux = v[i];
        v[i] = v[j];
        v[j] = aux;
        i++;
        j--;
      }
    } while (i <= j);
    quickInt(v,in,j);
    quickInt(v,i,f);
  } else {
    for (i = 1; i < f+1; i++) {
      pivo = v[i];
      j = i-1;
      if (strcmp(v[j].nome,pivo.nome)>0) {
        for (; j>=0 && strcmp(v[j].nome,pivo.nome)>0;j--){
          v[j+1] = v[j];
        }
        v[j+1] = pivo;
      }
    }
  }
}

void quickSort(tipoDados v[], int t) {
  quickInt(v,0,t-1);
}
