/*
  @author: Nabson Paiva
  @schoolSubject: AEDI - Ciência da Computação
  @question: Faça uma função que receba como parâmetro uma lista
    e retorne a soma dos valores presentes na lista encadeada. A
    função deve retornar zero para listas vazias.
*/

int soma(tipoLista l){
  int s = 0;
  while (l.prim){
    s += l.prim->dado;
    l.prim = l.prim->prox;
  }
  return s;
}
