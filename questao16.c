/*
  @author: Nabson Paiva
  @schoolSubject: AEDI - Ciência da Computação
  @question: Faca uma função que destrua uma lista encadeada passada
    como parâmetro, apagando todos os elementos da lista. A função não
    pode deixar qualquer nó da lista alocado na memória.
*/

void destroi(tipoLista *l) {
  tipoNo *aux;
  while(l->prim) {
    aux = l->prim;
    l->prim = l->prim->prox;
    free(aux);
  }
  l->prim = NULL;
}
