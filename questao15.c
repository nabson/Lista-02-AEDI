/*
  @author: Nabson Paiva
  @schoolSubject: AEDI - Ciência da Computação
  @question: Faça uma função que retorne 1 (verdadeiro) caso uma lista
    encadeada passada como parâmetro tenha nós com valores estritamente
    crescentes (elemento da posição i < elemento da posição j, para todo
    i<j). A função deve retornar 0 (falso) em caso contrário. Considere
    que listas vazias e com 1 elemento são estritamente crescentes.
*/

int isCrescente(tipoLista l) {
  if (l.prim) {
    while(l.prim->prox) {
      if (l.prim->dado >= l.prim->prox->dado)
        return 0;
      l.prim = l.prim->prox;
    }
  }
  return 1;
}
