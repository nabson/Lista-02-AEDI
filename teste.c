#include <stdio.h>
#include <string.h>
#include <stdlib.h>

typedef struct {
  char nome[20];
  char endereco[40];
  int id;
} tipoDados;

/*typedef struct tipoNo{
  tipoDados d;
  struct tipoNo *prox;
} tipoNo;*/

typedef struct tipoNo{
  int dado;
  struct tipoNo *prox;
} tipoNo;

typedef struct {
  tipoNo *prim;
} tipoLista;

void criar(tipoLista *l){
  l->prim = NULL;
}

int inserir(tipoLista *l, int d) {
  tipoNo *aux = (tipoNo *) malloc(sizeof(tipoNo)*8), *passo;
  if (aux) {
    aux->dado = d;
    aux->prox = NULL;
    if (!l->prim){
      l->prim = aux;
      return 1;
    }
    passo = l->prim;
    while (passo->prox)
      passo = passo->prox;
    passo->prox = aux;
    return 1;
  } else return 0;
}

void mostrar(tipoLista l) {
  while (l.prim) {
    printf("%d\n", l.prim->dado);
    l.prim = l.prim->prox;
  }
}

void destroi(tipoLista *l) {
  tipoNo *aux;
  while(l->prim) {
    aux = l->prim;
    l->prim = l->prim->prox;
    free(aux);
  }
  l->prim = NULL;
}

int main() {
  tipoLista l;
  criar(&l);
  inserir(&l,84);
  inserir(&l,84);
  inserir(&l,84);
  inserir(&l,84);
  inserir(&l,84);
  inserir(&l,84);
  /*tipoDados d;
  d.id = 01;
  strcpy(d.nome,"Nabs");
  strcpy(d.endereco,"Betania");
  inserir(&l,d);
  d.id = 02;
  strcpy(d.nome,"Nat");
  strcpy(d.endereco,"Betania");
  inserir(&l,d);
  d.id = 03;
  strcpy(d.nome,"Nat");
  strcpy(d.endereco,"Betania");
  inserir(&l,d);
  d.id = 04;
  strcpy(d.nome,"Nat");
  strcpy(d.endereco,"Betania");
  inserir(&l,d);
  d.id = 05;
  strcpy(d.nome,"Francy");
  strcpy(d.endereco,"Betania");
  inserir(&l,d);*/
  mostrar(l);
  destroi(&l);
  printf("***********\n");
  mostrar(l);
  return 0;
}
